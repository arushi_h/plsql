drop database if exists conference_room;

create database conference_room;

use conference_room;

drop table if exists HALLS;

create table HALLS(
HName varchar(4) primary key,
HType varchar(10),
Rent integer,
NoOfBookings integer,
check(Rent>=0)
);

drop table if exists COMPANY;

create table COMPANY(
CCode integer primary key,
CompanyName varchar(30),
ContactNo BIGINT 
);

 
drop table if exists BOOKING ;

create table BOOKING(
TransId integer PRIMARY KEY AUTO_INCREMENT ,
CCode integer,
HName varchar(4),
StartDate DATE,
EndDate DATE,
FOREIGN KEY(CCode) references COMPANY(CCode),
FOREIGN KEY(HName) references HALLS(HName) 
);

alter table BOOKING auto_increment = 101;




